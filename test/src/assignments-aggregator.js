/**
 * Created by Magdalena on 2015-02-12.
 */

var _ = require('lodash');
var React = require('react');
var Label = require('react-bootstrap').Label;

var createTooltip = function(assignments, args) {
    args = args || {};
    var countedAssignments = {};

    if (args.view === 'course') {
        countedAssignments = _.countBy(assignments, 'assignmentId');
        assignments = _.uniq(assignments, function(a) {
            return a.assignmentId;
        });
        for (var i = 0; i < assignments.length; ++i) {
            assignments[i].count = countedAssignments[assignments[i].assignmentId];
        }
    }

    var lines = assignments.map(function(a) {
        var label = null;
        var counter = null;

        if (args.view === 'course') {
            counter = "[" + a.count + "] ";
        } else {
            if (a.derivedStatus !== "n/a") {
                label = React.createElement(Label, {bsStyle: "default"}, a.derivedStatus);
            }
        }
        return React.createElement("li", {key: 'id' + a.id}, counter, React.createElement("a", {href: a.assignmentHtmlUrl}, a.assignmentName), label)
    });

    return React.createElement("ul", null, lines)
};

var aggregateData = function(rawData, args) {
    args = args || {};
    var dateKey = "assignmentDueAt";

    var groupedData = _.groupBy(rawData, function(a) {
        var date = new Date(a[dateKey]);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }, this);

    var data = [];
    var tooltips = [];

    for (var key in groupedData) {
        if (groupedData.hasOwnProperty(key)) {
            data.push({
                date: new Date(key),
                value: groupedData[key].length
            });

            tooltips.push({
                date: new Date(key),
                tooltip: createTooltip(groupedData[key], {view: args.view})
            });
        }
    }

    return {
        data: data,
        tooltips: tooltips
    }
};

exports.aggregateData = aggregateData;