/** @jsx React.DOM */
var React = require('react/addons');
var FilteredCalendar = require('./filtered-calendar');

var App = React.createClass({displayName: "App",
    render: function() {
        return (
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-sm-12"}, 
                    React.createElement(FilteredCalendar, {
                        source: "https://mam.eastsideprep.org/api/v1/assignment_submissions.json?ids%5B%5D=925447&ids%5B%5D=925477&ids%5B%5D=918550&ids%5B%5D=918549&ids%5B%5D=907740&ids%5B%5D=907704&ids%5B%5D=923186&ids%5B%5D=907298&ids%5B%5D=886898&ids%5B%5D=924902&ids%5B%5D=917508&ids%5B%5D=924901&ids%5B%5D=919955&ids%5B%5D=881815&ids%5B%5D=910595&ids%5B%5D=924627&ids%5B%5D=923187&ids%5B%5D=856904&ids%5B%5D=920519&ids%5B%5D=924051&ids%5B%5D=923379&ids%5B%5D=919947&ids%5B%5D=856903&ids%5B%5D=920518&ids%5B%5D=922675&ids%5B%5D=920407&ids%5B%5D=923188&ids%5B%5D=907297&ids%5B%5D=920589&ids%5B%5D=856901&ids%5B%5D=893591&ids%5B%5D=917501&ids%5B%5D=918911&ids%5B%5D=919474&ids%5B%5D=918897&ids%5B%5D=886902&ids%5B%5D=914592&ids%5B%5D=856902&ids%5B%5D=919452&ids%5B%5D=918548&ids%5B%5D=918305&ids%5B%5D=914197&ids%5B%5D=916621&ids%5B%5D=918032&ids%5B%5D=911490&ids%5B%5D=917465&ids%5B%5D=907296&ids%5B%5D=917222", 
                        view: "student"}
                    )
                )
            )
        )
    }
});



React.render(
    React.createElement(App, null),
    document.getElementById('calendar')
);
