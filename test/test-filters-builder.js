var rewire = require('rewire');
var testdata = require('./src/testdata');
var _ = require('lodash');

var filtersBuilder = rewire('./src/filters-builder');

var compareForFilter = function(util, customEqualityTesters, actual) {
    if (!actual.hasOwnProperty('displayName')) {
        return {
            pass: false,
            message: 'No displayName property'
        }
    }
    if (!util.equals(actual.displayName, jasmine.any(String), customEqualityTesters)) {
        return {
            pass: false,
            message: 'Property displayName is not a String'
        }
    }
    if (!actual.hasOwnProperty('test')) {
        return {
            pass: false,
            message: 'No test property'
        }
    }
    if (!_.isFunction(actual.test)) {
        return {
            pass: false,
            message: 'Property test is not a function'
        }
    }
    if (!actual.hasOwnProperty('options')) {
        return {
            pass: false,
            message: 'No options property'
        }
    }
    if (!util.equals(actual.options, jasmine.any(Array), customEqualityTesters)) {
        return {
            pass: false,
            message: 'Property options is not an array'
        }
    }
    return {
        pass: true
    };
};

var compareForFilterWithOptions = function(util, customEqualityTesters, actual) {
    var result = compareForFilter(util, customEqualityTesters, actual);
    if (!result.pass)
        return result;
    if (actual.options.length === 0) {
        return {
            pass: false,
            message: 'Options array is empty'
        }
    }
    for (var i = 0; i < actual.options.length; ++i) {
        if (!actual.options[i].hasOwnProperty('displayName')) {
            return {
                pass: false,
                message: 'No displayName property in option'
            };
        }
        if (!actual.options[i].hasOwnProperty('include')) {
            return {
                pass: false,
                message: 'No include property in option'
            }
        }
        if (!util.equals(actual.options[i].displayName, jasmine.any(String), customEqualityTesters)) {
            return {
                pass: false,
                message: 'Property displayName is not a String'
            }
        }
        if (!util.equals(actual.options[i].include, jasmine.any(Boolean), customEqualityTesters)) {
            return {
                pass: false,
                message: 'Property include is not a Boolean'
            }
        }
    }

    return {
        pass: true
    }
};

var customMatchers = {
    toBeFilter: function(util, customEqualityTesters) {
        return {
            compare: compareForFilter.bind(this, util, customEqualityTesters)
        }
    },

    toBeFilterWithOptions: function(util, customEqualityTesters) {
        return {
            compare: compareForFilterWithOptions.bind(this, util, customEqualityTesters)
        }
    }
};

var anyFilter = {
    asymmetricMatch: function(actual) {
        if (!actual.hasOwnProperty('displayName'))
            return false;
        if (typeof actual.displayName != String)
            return false;
        if (!actual.hasOwnProperty('test'))
            return false;
        return true;
    }
};

describe('filters builder', function() {
    it('returns array ', function() {
        var filters = filtersBuilder.getFilters([]);

        expect(filters).toEqual(jasmine.any(Array));
    });

    describe('on testdata', function() {
        var data;
        var filters;

        beforeAll(function() {
            data = testdata.getRawData();
            filters = filtersBuilder.getFilters();
            jasmine.addMatchers(customMatchers);
        });

        it('returns non-empty array', function() {
            expect(filters.length).toBeGreaterThan(0);
        });

        it('returns filters with specified properties', function() {
            var filter = filters[0];
            expect(filter).toBeFilterWithOptions();
        });
    });

    describe('createBasicFilter', function() {
        var data;
        var createBasicFilter = filtersBuilder.__get__('createBasicFilter');
        var dictOne;
        var dictTwo;
        var dictOneTwo;

        beforeEach(function() {
            var one = {
                a: '1',
                b: '1'
            };
            var two = {
                a: '2',
                b: '1'
            };
            data = [one, two];
            dictOne = {
                '1': 'One'
            };
            dictTwo = {
                '2': 'Two'
            };
            dictOneTwo = {
                '1': 'One',
                '2': 'Two'
            };

            jasmine.addMatchers(customMatchers);
        });

        it('should throw error for undefined data and property params', function() {
            expect(createBasicFilter).toThrow();
        });

        it('should create filter with specified properties for well defined data and property', function() {
            var filter = createBasicFilter(data, 'a');
            expect(filter).toBeFilterWithOptions();
        });

        it('should create empty filter for empty data', function() {
            var emptyData = [];
            var property = 'property';
            var filter = createBasicFilter(emptyData, property);
            expect(filter).toBeFilter();
        });

        it('should create filter with one option', function() {
            var filter = createBasicFilter(data, 'b');
            expect(filter.options.length).toEqual(1);
        });

        it('should create filter with two options', function() {
            var filter = createBasicFilter(data, 'a');
            expect(filter.options.length).toEqual(2);
        });

        it('should create displayName when not specified', function() {
            var filter = createBasicFilter(data, 'a');
            expect(filter.displayName).toEqual('a');
        });

        it('should create displayName when specified', function() {
            var filter = createBasicFilter(data, 'a', 'A');
            expect(filter.displayName).toEqual('A');
        });

        it('should create options displayName when not specified', function() {
            var filter = createBasicFilter(data, 'b');
            expect(filter.options[0].displayName).toEqual('1');
        });

        it('should create two different options for two different properties', function() {
            var filter = createBasicFilter(data, 'a');
            expect(filter.options[0].displayName).not.toEqual(filter.options[1].displayName);
        });

        it('should create proper options displayName properties when specified', function() {
            var filter = createBasicFilter(data, 'b', 'B', dictOne);
            expect(filter.options[0].displayName).toEqual('One');
        });

        it('should create proper options displayName properties when partially specified', function() {
            var filter = createBasicFilter(data, 'a', 'A', dictTwo);
            var results = [filter.options[0].displayName, filter.options[1].displayName];
            expect(results).toContain('1');
            expect(results).toContain('Two');
        });

        it('should create passing test', function() {
            var filter = createBasicFilter(data, 'a');
            for (var i = 0; i < data.length; ++i) {
                expect(filter.test(data[i])).toBe(true);
            }
        });

        it('should create test that fails after toggling options include property', function() {
            var filter = createBasicFilter(data, 'b');
            filter.options[0].include = false;
            for (var i = 0; i < data.length; ++i) {
                expect(filter.test(data[i])).toBe(false);
            }
        });

        it('should create test that fails/passes on the basis of OR operator on options', function() {
            var filter = createBasicFilter(data, 'a');
            expect(filter.test(data[0])).toBe(true);
            expect(filter.test(data[1])).toBe(true);
            var indexOne = _.findIndex(filter.options, {displayName: '1'});
            var indexTwo = _.findIndex(filter.options, {displayName: '2'});
            filter.options[indexOne].include = false;
            expect(filter.test(data[0])).toBe(false);
            expect(filter.test(data[1])).toBe(true);
            filter.options[indexTwo].include = false;
            expect(filter.test(data[0])).toBe(false);
            expect(filter.test(data[1])).toBe(false);
            filter.options[indexOne].include = true;
            expect(filter.test(data[0])).toBe(true);
            expect(filter.test(data[1])).toBe(false);
        })
    });
});