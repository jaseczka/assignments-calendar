var React = require('react/addons');
var FilteredCalendar = require('../../src/filtered-calendar');
var TestUtils = React.addons.TestUtils;
var moment = require('moment');
var DatePicker = require('react-eps').DatePicker;

var customMatchers = {
    toHaveDateKey: function(util, customEqualityTesters) {
        return {
            compare: function(actual, expected) {
                var actualKey = actual._owner._currentElement.key;
                var keyDate = moment(actualKey);
                if (!keyDate.isSame(expected))
                    return {
                        pass: false,
                        message: keyDate + ' expected to be ' + expected
                    };
                return {
                    pass: true
                }
            }
        }
    }
};

describe('Calendar', function() {
    var Calendar = FilteredCalendar.__get__('Calendar');
    var calendar, dateFrom, dateTo, calendarData, modalData;

    beforeEach(function() {
        jasmine.addMatchers(customMatchers);
        dateFrom = moment().year(2015).month(0).date(1).startOf('day');
        dateTo = moment().year(2015).month(0).date(31).startOf('day');
        calendarData = [
            {
                date: dateFrom.toDate(),
                value: 1
            }
        ];
        modalData = [
            {
                date: dateFrom.toDate(),
                tooltip: null
            }
        ];
        calendar = TestUtils.renderIntoDocument(
            <Calendar
                initialDateFrom={dateFrom}
                initialDateTo={dateTo}
                calendarData={calendarData}
                modalData={modalData}
            />
        );
    });

    it('renders 2 datepickers', function() {
        var datepickers = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'datepicker');
        expect(datepickers.length).toEqual(2);
    });

    it('renders date in \'from\' datepicker according to initialDateFrom property', function() {
        var datepicker = TestUtils.findRenderedDOMComponentWithClass(calendar, 'datepicker-from');
        var dateFormat = TestUtils.findRenderedComponentWithType(datepicker, DatePicker).props.dateFormat;
        var datepickerInput = TestUtils.findRenderedDOMComponentWithClass(datepicker, 'datepicker__input');
        var actual = datepickerInput.getDOMNode().value;
        expect(actual).toEqual(dateFrom.format(dateFormat));
    });

    it('renders date in \'to\' datepicker according to initialDateTo property', function() {
        var datepicker = TestUtils.findRenderedDOMComponentWithClass(calendar, 'datepicker-to');
        var dateFormat = TestUtils.findRenderedComponentWithType(datepicker, DatePicker).props.dateFormat;
        var datepickerInput = TestUtils.findRenderedDOMComponentWithClass(datepicker, 'datepicker__input');
        var actual = datepickerInput.getDOMNode().value;
        expect(actual).toEqual(dateTo.format(dateFormat));
    });

    it('renders first day in calendar with key equal to initialDateFrom property', function() {
        var days = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'chm-day');
        expect(days[0]).toHaveDateKey(dateFrom);
    });

    it('renders last day in calendar with key equal to initialDateTo property', function() {
        var days = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'chm-day');
        expect(days[days.length - 1]).toHaveDateKey(dateTo);
    });

    it('renders right amount of days in calendar', function() {
        var days = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'chm-day');
        var actualNumber = days.length;
        var expectedNumber = dateTo.diff(dateFrom, 'days') + 1;
        expect(actualNumber).toEqual(expectedNumber);
    });

    describe('when \'from\' datepicker input field changes', function() {
        var days;
        var newFirstDay;

        beforeEach(function() {
            var datepickerLabel = TestUtils.findRenderedDOMComponentWithClass(calendar, 'datepicker-from');
            var dateFormat = TestUtils.findRenderedComponentWithType(datepickerLabel, DatePicker).props.dateFormat;
            var datepickerInput = TestUtils.findRenderedDOMComponentWithClass(datepickerLabel, 'datepicker__input');
            var inputNode = datepickerInput.getDOMNode();
            newFirstDay = dateFrom.add(1, 'day');

            TestUtils.Simulate.click(inputNode);
            TestUtils.Simulate.change(inputNode, {target: {value: newFirstDay.format(dateFormat)}});
            TestUtils.Simulate.keyDown(inputNode, {key: 'Enter'});

            days = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'chm-day');
        });

        it('first day in calendar changes accordingly', function() {
            expect(days[0]).toHaveDateKey(newFirstDay);
        });

        it('last day in calendar doesn\'t change', function() {
            expect(days[days.length-1]).toHaveDateKey(dateTo);
        });

        it('number of calendar days changes accordingly', function() {
            expect(days.length).toEqual(dateTo.diff(newFirstDay, 'days') + 1);
        });

    });

    describe('when \'to\' datepicker input field changes', function() {
        var days;
        var newLastDay;

        beforeEach(function() {
            var datepickerLabel = TestUtils.findRenderedDOMComponentWithClass(calendar, 'datepicker-to');
            var dateFormat = TestUtils.findRenderedComponentWithType(datepickerLabel, DatePicker).props.dateFormat;
            var datepickerInput = TestUtils.findRenderedDOMComponentWithClass(datepickerLabel, 'datepicker__input');
            var inputNode = datepickerInput.getDOMNode();
            newLastDay = dateFrom.add(1, 'day');

            TestUtils.Simulate.click(inputNode);
            TestUtils.Simulate.change(inputNode, {target: {value: newLastDay.format(dateFormat)}});
            TestUtils.Simulate.keyDown(inputNode, {key: 'Enter'});

            days = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'chm-day');
        });

        it('first day in calendar changes accordingly', function() {
            expect(days[0]).toHaveDateKey(dateFrom);
        });

        it('last day in calendar doesn\'t change', function() {
            expect(days[days.length-1]).toHaveDateKey(newLastDay);
        });

        it('number of calendar days changes accordingly', function() {
            expect(days.length).toEqual(newLastDay.diff(dateFrom, 'days') + 1);
        });

    });

    it('doesn\'t display modal window when clicked on a day with no deadlines', function() {
        var days = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'chm-day');
        TestUtils.Simulate.click(days[1].getDOMNode());
        var modals = document.getElementsByClassName('modal-dialog');
        expect(modals.length).toEqual(0);
    });

    it('displays modal window when clicked on a day with deadlines', function() {
        var days = TestUtils.scryRenderedDOMComponentsWithClass(calendar, 'chm-day');
        TestUtils.Simulate.click(days[0].getDOMNode());
        var modals = document.getElementsByClassName('modal-dialog');
        expect(modals.length).toEqual(1);
        //cleanup
        var close = modals[0].getElementsByClassName('close');
        TestUtils.Simulate.click(close[0]);
    });

});

describe('FilteredCalendar', function() {

    beforeEach(function() {
        filteredCalendar = TestUtils.renderIntoDocument(
            <FilteredCalendar />
        );
    });

    it('renders calendar', function() {
        var calendar = TestUtils.findRenderedDOMComponentWithClass(filteredCalendar, 'calendar-heat-map');
        expect(calendar).toBeDefined();
    });

    it('renders filter panel', function() {
        var filterPanel = TestUtils.findRenderedDOMComponentWithClass(filteredCalendar, 'filter-panel');
        expect(filterPanel).toBeDefined();
    });

    it('renders filter sets', function() {
        var filterSets = TestUtils.findRenderedDOMComponentWithClass(filteredCalendar, 'filter-sets');
        var filterSetsButtons = TestUtils.scryRenderedDOMComponentsWithClass(filteredCalendar, 'filter-set-button');
        expect(filterSets).toBeDefined();
        expect(filterSetsButtons.length).toBeGreaterThan(0);
    });

    it('')
});