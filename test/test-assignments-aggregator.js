var aggregator = require('./src/assignments-aggregator');
var testdata = require('./src/testdata');
var React = require('react/addons');

describe('assignments aggregator', function() {

    describe('on empty array', function() {
        var rawData;
        var aggregated;

        beforeAll(function() {
            rawData = [];
            aggregated = aggregator.aggregateData(rawData);
        });

        it('should return object with data and tooltips arrays', function() {
            expect(aggregated.hasOwnProperty('data')).toBe(true);
            expect(aggregated.hasOwnProperty('tooltips')).toBe(true);
            expect(aggregated.data).toEqual(jasmine.any(Array));
            expect(aggregated.tooltips).toEqual(jasmine.any(Array));
        });

        it('should return empty data and tooltips arrays', function() {
            expect(aggregated.data).toEqual([]);
            expect(aggregated.tooltips).toEqual([]);
        });
    });

    describe('on one element array', function() {
        var rawData;
        var aggregated;

        beforeAll(function() {
            rawData = [testdata.getOneAssignment()];
            aggregated = aggregator.aggregateData(rawData);
        });

        it('should return one element in data and tooltips arrays', function() {
            expect(aggregated.data.length).toEqual(1);
            expect(aggregated.tooltips.length).toEqual(1);
        });

        it('should return data array of objects containing date and value properties', function() {
            expect(aggregated.data[0].hasOwnProperty('date')).toBe(true);
            expect(aggregated.data[0].date).toEqual(jasmine.any(Date));
            expect(aggregated.data[0].hasOwnProperty('value')).toBe(true);
            expect(aggregated.data[0].value).toEqual(jasmine.any(Number));
            expect(aggregated.data[0].value).toBeGreaterThan(0);
        });

        it('should return tooltips array of objects containing date and tooltip properties', function() {
            expect(aggregated.tooltips[0].hasOwnProperty('date')).toBe(true);
            expect(aggregated.tooltips[0].date).toEqual(jasmine.any(Date));
            expect(aggregated.tooltips[0].hasOwnProperty('tooltip')).toBe(true);
            expect(React.addons.TestUtils.isElement(aggregated.tooltips[0].tooltip)).toBe(true);
        });
    });

    it('should not aggregate two elements with different dates', function() {
        var dateOne = new Date(2015, 0, 1);
        var dateTwo = new Date(2015, 0, 2);
        var rawData = [
            testdata.getOneAssignmentWithDate(dateOne),
            testdata.getOneAssignmentWithDate(dateTwo)
        ];
        var aggregated = aggregator.aggregateData(rawData);

        expect(aggregated.data.length).toEqual(2);
        expect(aggregated.tooltips.length).toEqual(2);
        expect(aggregated.data[0].value).toEqual(1);
        expect(aggregated.data[1].value).toEqual(1);
    });

    it('should aggregate two elements with the same date', function() {
        var date = new Date(2015, 0, 1);
        var rawData = [
            testdata.getOneAssignmentWithDate(date),
            testdata.getOneAssignmentWithDate(date)
        ];
        var aggregated = aggregator.aggregateData(rawData);

        expect(aggregated.data.length).toEqual(1);
        expect(aggregated.tooltips.length).toEqual(1);
        expect(aggregated.data[0].value).toEqual(2);
    });

    it('should aggregate two elemets with the same date but different hour', function() {
        var dateOne = new Date(2015, 0, 1, 0);
        var dateTwo = new Date(2015, 0, 1, 23);
        var rawData = [
            testdata.getOneAssignmentWithDate(dateOne),
            testdata.getOneAssignmentWithDate(dateTwo)
        ];
        var aggregated = aggregator.aggregateData(rawData);

        expect(aggregated.data.length).toEqual(1);
        expect(aggregated.tooltips.length).toEqual(1);
        expect(aggregated.data[0].value).toEqual(2);
    });

    describe('on testdata', function() {
        var rawData;
        var aggregated;

        beforeAll(function() {
            rawData = testdata.getRawData();
            aggregated = aggregator.aggregateData(rawData);
        });

        it('should aggregate (see test code)', function() {
            expect(aggregated.data.length).toEqual(7);
            expect(aggregated.tooltips.length).toEqual(7);
            var sorted = aggregated.data.slice().sort(function(a, b) {return a.date - b.date});
            expect(sorted).toEqual([
                { date: new Date(2015, 0, 6), value: 2 },
                { date: new Date(2015, 0, 8), value: 2 },
                { date: new Date(2015, 0, 9), value: 8 },
                { date: new Date(2015, 0, 13), value: 1 },
                { date: new Date(2015, 0, 14), value: 4 },
                { date: new Date(2015, 0, 16), value: 2 },
                { date: new Date(2015, 0, 21), value: 1 }
            ]);
        });

    });

    describe('with view: "course" property', function() {
        var twoAssignments, aggregated;

        beforeEach(function() {
            twoAssignments = [testdata.getOneAssignment(), testdata.getOneAssignment()];
            aggregated = aggregator.aggregateData(twoAssignments, {view: "course"});
        });

        it('should aggregate two assignments with the same assignmentId', function() {
            expect(aggregated.data.length).toEqual(1);
            expect(aggregated.tooltips.length).toEqual(1);
            expect(aggregated.data[0].value).toEqual(2);
        });
    })
});