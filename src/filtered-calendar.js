var React = require('react/addons');
var _ = require('lodash');
var DatePicker = require('react-eps').DatePicker;
var CalendarHeatMap = require('react-eps').CalendarHeatMap;
var FilterPanel = require('react-eps').FilterPanel;
var FilterPanelMixin = require('react-eps').FilterPanelMixin;
var Modal = require('react-bootstrap').Modal;
var OverlayMixin = require('react-bootstrap').OverlayMixin;
var Panel = require('react-bootstrap').Panel;
var testdata = require('./testdata');
var filtersBuilder = require('./filters-builder');
var aggregator = require('./assignments-aggregator');
var moment = require('moment');

var FilterSets = React.createClass({
    getInitialState: function() {
        return {
            filterSets: [{
                displayName: "Only Submitted",
                filters: [{
                    displayName: "Status",
                    options: [
                        {displayName: "n/a", include: false},
                        {displayName: "submitted", include: true},
                        {displayName: "pending", include: false},
                        {displayName: "upcoming", include: false},
                        {displayName: "missing", include: false}
                    ]
                }]
            }]
        }
    },

    render: function() {
        var buttons = this.state.filterSets.map(function(filterSet) {
            return (
                <button
                    className="btn btn-default filter-set-button"
                    onClick={this.props.toggleFilters.bind(null, filterSet.filters)}
                    key={filterSet.displayName}
                >
                {filterSet.displayName}
                </button>
            )
        }, this);

        return (
            <span className="filter-sets">
                {buttons}
            </span>
        )
    }
});

var FilteredCalendar = React.createClass({
    mixins: [FilterPanelMixin],

    getInitialState: function() {
        return {
            rawData: [],
            filters: filtersBuilder.getFilters([])
        }
    },

    getToggledFilters: function(oldFilters, toggled) {
        var mutation = {};

        for (var f = 0; f < toggled.length; ++f) {
            var optionsMutation = {};
            var propertyName = toggled[f].displayName;
            var filterIndex = _.findIndex(oldFilters, {displayName: propertyName});
            if (filterIndex >= 0) {
                var filter = oldFilters[filterIndex];
                for (var i = 0; i < toggled[f].options.length; ++i) {
                    var optionName = toggled[f].options[i].displayName;
                    var optionIndex = _.findIndex(filter.options, {displayName: optionName});
                    if (optionIndex >= 0) {
                        optionsMutation[optionIndex] = {include: {$set: toggled[f].options[i].include}}
                    }
                }
                mutation[filterIndex] = {options: optionsMutation};
            }
        }

        var newFilters = React.addons.update(oldFilters, mutation);

        return newFilters;
    },

    toggleFilters: function(filters) {
        var newFilters = this.getToggledFilters(this.state.filters, filters);
        this.setState({
            filters: newFilters
        })
    },

    turnOnAllFilters: function() {
        var mutation = {};
        for (var f = 0; f < this.state.filters.length; ++f) {
            var optionsMutation = {};
            for (var i = 0; i < this.state.filters[f].options.length; ++i) {
                optionsMutation[i] = {include: {$set: true}};
            }
            mutation[f] = {options: optionsMutation};
        }

        var newFilters = React.addons.update(this.state.filters, mutation);

        this.setState({
            filters: newFilters
        });
    },

    loadAssignmentsData: function() {
        if(!this.props.source) {
            var data = testdata.getRawData();
            var filters = filtersBuilder.getFilters(data);

            this.setState({
                rawData: data,
                filters: filters
            });
            return;
        }

        $.ajax({
            url: this.props.source,
            dataType: 'json',
            success: function(data) {
                var assignments = data.assignment_submissions;
                var filters = filtersBuilder.getFilters(assignments);
                var changed = this.getToggledFilters(filters, this.state.filters);
                if (this.isMounted()) {
                    this.setState({
                        rawData: assignments,
                        filters: changed
                    });
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.source, status, err.toString());
            }.bind(this)
        });
    },

    componentDidMount: function() {
        this.loadAssignmentsData();
        setInterval(this.loadAssignmentsData, 120000);
    },

    render: function() {
        /* getFilteredData from FilterPanelMixin */
        var filteredData = this.getFilteredData(this.state.rawData);
        var aggregatedData = aggregator.aggregateData(filteredData, {view: this.props.view});

        var calendarData = aggregatedData.data;
        var modalData = aggregatedData.tooltips;
        return (
            <div>
                <h3>Assignments Calendar</h3>
                <Panel collapsable defaultExpanded header="Filter Sets" >
                    <button
                        className="btn btn-default filter-set-button"
                        onClick={this.turnOnAllFilters}
                    >
                        All
                    </button>
                    <FilterSets toggleFilters={this.toggleFilters} />
                </Panel>
                <Panel collapsable header="Detailed Filters">
                    {this.renderFilterPanel({toggleAllEnabled: true})}
                </Panel>
                <Calendar
                    calendarData={calendarData}
                    modalData={modalData}
                    initialDateFrom={moment().year(2015).month(0).date(1)}
                />
            </div>
        )
    }
});

var Calendar = React.createClass({
    //bootstrap mixin that allows for custom modal state management
    mixins: [OverlayMixin],

    getDefaultProps: function() {
        return {
            initialDateFrom: moment(),
            initialDateTo: moment().add(28, 'd')
        }
    },

    getInitialState: function() {
        return {
            isModalVisible: false,
            dateFrom: this.props.initialDateFrom,
            dateTo: this.props.initialDateTo
        };
    },

    openModal: function(date) {
        this.setState({
            isModalOpen: true,
            modalDate: date
        });
    },

    closeModal: function() {
        this.setState({
            isModalOpen: false
        });
    },

    changeDateFrom: function(moment) {
        this.setState({dateFrom: moment});
    },

    changeDateTo: function(moment) {
        this.setState({dateTo: moment});
    },

    render: function() {
        return (
            <div>
                <div>
                    <label className="datepicker-from">from
                        <DatePicker
                            selected={this.state.dateFrom}
                            onChange={this.changeDateFrom}
                            dateFormat="MM/DD/YYYY"
                            maxDate={this.state.dateTo}
                        />
                    </label>

                    <label className="datepicker-to">to
                        <DatePicker
                            selected={this.state.dateTo}
                            onChange={this.changeDateTo}
                            dateFormat="MM/DD/YYYY"
                            minDate={this.state.dateFrom}
                        />
                    </label>
                </div>

                <CalendarHeatMap
                    handleClick={this.openModal}
                    data={this.props.calendarData}
                    mode="daily"
                    dateFrom={this.state.dateFrom.toDate()}
                    dateTo={this.state.dateTo.toDate()}
                    counterEnabled={true}
                    title=""
                />
            </div>
        );
    },

    //OverlayMixin render function uses this:
    renderOverlay: function() {
        if (!this.state.isModalOpen) {
            return <span />;
        }
        var tooltip = _.find(this.props.modalData, {date: this.state.modalDate});
        //Don't open modal for a day without any deadlines:
        if (tooltip === undefined) {
            return <span />;
        }

        return (
            <Modal
                title={"Assignments due " + moment(tooltip.date).format('dddd, MMMM Do YYYY')}
                onRequestHide={this.closeModal}
            >
                <div className="modal-body">
                {tooltip.tooltip}
                </div>
                <div className="modal-footer">
                    <button className="btn btn-default" onClick={this.closeModal}>Close</button>
                </div>
            </Modal>
        )

    }
});

module.exports = FilteredCalendar;
