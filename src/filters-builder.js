/**
 * Created by Magdalena on 2015-02-12.
 */


var _ = require('lodash');

var simpleTestProperty = function(assignment) {
    if (assignment.hasOwnProperty(this.property)) {
        var testedValue = assignment[this.property];
        //at least one of the options tests must return true
        for (var i = 0; i < this.options.length; ++i) {
            if (this.options[i].include && this.options[i].test(testedValue)) {
                return true;
            }
        }
        return false;
    }
};

var createBasicFilter = function(data, property, displayName, dictionary) {
    if (!data || !property)
        throw new Error('Undefined parameters');

    displayName = displayName || String(property);
    dictionary = dictionary || {};

    var allValues = _.pluck(data, property);
    var uniqueValues = _.uniq(allValues);
    var options = [];
    _.forEach(uniqueValues, function(value) {
        var displayName = String(value);
        if (dictionary.hasOwnProperty(value)) {
            displayName = String(dictionary[value]);
        }
        options.push({
            displayName: displayName,
            test: function(that) {
                return (that === value);
            },
            include: true
        })
    });

    return({
        displayName: displayName,
        test: simpleTestProperty,
        property: property,
        options: options
    });
};

var createBasicFilters = function(data, properties, dictionary) {
    if (!data || !properties)
        return null;

    dictionary = dictionary || {};

    var filters = [];

    for (var i = 0; i < properties.length; ++i) {
        var filter = createBasicFilter(data, properties[i], dictionary[properties[i]]);
        filters.push(filter);
    }

    return filters;
};

var createStartsWithFilter = function(data, property, displayName, startOptions) {

    var options = startOptions.map(function(str) {
        return {
            displayName: str,
            include: true,
            test: function(text) {
                return (text.indexOf(str) === 0);
            }
        }
    });

    options.push({
        displayName: "other",
        include: true,
        test: function(text) {
            for (var i = 0; i < startOptions.length; ++i) {
                if (text.indexOf(startOptions[i]) === 0)
                    return false;
            }
            return true;
        }
    });

    return {
        displayName: displayName,
        test: simpleTestProperty,
        property: property,
        options: options
    }
};

var createRegexFilter = function(data, property, displayName, regexOptions) {

    var options = regexOptions.map(function(option) {
        return {
            displayName: option.displayName,
            include: true,
            test: function(text) {
                return option.re.test(text);
            }
        }
    });

    options.push({
        displayName: "other",
        include: true,
        test: function(text) {
            for (var i = 0; i < regexOptions.length; ++i) {
                if (regexOptions[i].re.test(text))
                    return false;
            }
            return true;
        }
    });

    return {
        displayName: displayName,
        test: simpleTestProperty,
        property: property,
        options: options
    }
};

var getFilters = function(data) {
    data = data || assignments;
    var filters = createBasicFilters(
        data,
        ["derivedStatus", "submissionLate", "submissionType", "assignmentSubmissionTypes"],
        {
            "derivedStatus": "Status",
            "submissionType": "Submission Type"
        }
    );
    var regexFilter = createRegexFilter(
        data,
        "assignmentName",
        "Assignment Type",
        [
            {displayName: "CW", re: /^\s*CW:?\b/i},
            {displayName: "HW", re: /^\s*HW:?\b/i},
            {displayName: "MA", re: /^\s*MA:?\b/i}
        ]
    );
    filters.push(regexFilter);
    var courseFilter = createBasicFilter(
        data,
        "course",
        "Course",
        {
            "1411515": "course 1",
            "1411565": "another",
            "1411543": "blah"
        }
    );
    filters.push(courseFilter);
    return filters;
};

exports.getFilters = getFilters;