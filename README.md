# README #

This is a project for Eastside Preparatory School. It's aim is to create a calendar heat map of assignments deadlines for students. It features a [react-eps](https://bitbucket.org/jaseczka/assignments-calendar) node module, which is a set of reusable and customizable components created for EPS.

Currently, application renders FilteredCalendar component that takes two parameters:

### source ###
Url which is used for ajax request to get assignments data.

### view ###
Default view is `"student"`. It may be switched to `"course"`, which would aggregate assignments with the same `assignmentId` and display their count in modal window.

## Demo ##

[Here](http://students.mimuw.edu.pl/~mt266699/assignments-calendar/) is a live version that may be a few commits behind the repo. In order to run the newest version download/clone/npm install repository and open dist/index.html

## Developing ##

```
#!bash
$ git clone https://jaseczka@bitbucket.org/jaseczka/assignments-calendar.git
$ cd assignments-calendar
$ npm install
$ npm install react

```

There is a set of predefined npm commands, see "scripts" in package.json. For example, in order to see your changes in dist/index.html use `npm run build` or `npm run watch`.

## Developing simultaneously with react-eps ##

1. Clone & npm install react-eps
2. Clone assignments-calendar
3. `npm link` react-eps to assignments-calendar (see npm link docs)
4. `npm install` assignments-calendar
5. You may use `npm run watch-react-eps` and `npm run watch` to instantly view changes in dist/index.html

## Testing ##

For testing React-less code in command line you may use `npm run test` (needed global npm jasmine).

Tests for React components are under development (see react-testing branch)